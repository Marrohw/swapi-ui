import type { AxiosResponse } from 'axios';
import { httpClient } from './httpClient';

type Method = 'get';

type Params = {
  readonly method: Method;
  readonly url: string;
  readonly body?: unknown;
};

const methodToCallHttpClient: Record<Method, ({ url, body }: Pick<Params, 'url' | 'body'>) => Promise<AxiosResponse>> =
  {
    get: ({ url }) => httpClient.get(url),
  };

async function callApi({ method, url, body }: Params) {
  const basePath = 'https://swapi.dev/';

  return methodToCallHttpClient[method]({
    url: basePath + url,
    body,
  });
}

export { callApi };
