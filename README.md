
Swapi-ui is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app) based on the [SWAPI API](https://swapi.dev/)

## Getting Started

First, run the development server:

```bash

npm run  dev

# or

yarn dev

# or

pnpm dev

```

Open [http://localhost:3000/starships](http://localhost:3000/starships) with your browser to see the result.

## Features

- Displaying starships with pagination (visit localhost:3000/starships)
- Displaying starship detail
- Filter starships by number of passengers and Hyperdrive rating

## Possible improvements

- Adding [Skeleton loading](https://chakra-ui.com/docs/components/skeleton/usage) with Chakra-ui
- Use [react-query](https://www.npmjs.com/package/@tanstack/react-query) instead of native fetch
- Use [react-hook-form](https://react-hook-form.com/) for the form filtering
- Create specific hooks like useGetStarships (see TODOs)
- Make more filters
- Display more information

## Linting, building...

```bash

npm run lint

# or

yarn lint

# or

pnpm lint

```

```bash

npm run build

# or

yarn build

# or

pnpm build

```

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.

- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.