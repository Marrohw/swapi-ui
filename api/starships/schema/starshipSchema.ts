import { z } from 'zod';

const starshipDetailSchema = z.object({
    name: z.string(),
    model: z.string(),
    length: z.number(),
    passengers: z.number(),
    manufacturer: z.string(),
    url: z.string(),
    films: z.array(z.string()),
    consumables: z.string(),
    crew: z.string(),
    hyperdrive_rating: z.string(),
})

const starshipSchema = z.object({
  count: z.number(),
  next: z.string().nullable(),
  previous: z.string().nullable(),
  results: z.array(starshipDetailSchema)
});

type Starship = z.infer<typeof starshipSchema>;
type StarshipDetail = z.infer<typeof starshipDetailSchema>;

export type { Starship, StarshipDetail };
export { starshipSchema };
