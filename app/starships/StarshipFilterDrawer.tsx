'use client';
import { Box, Button, Drawer, DrawerBody, DrawerCloseButton, DrawerContent, DrawerFooter, DrawerHeader, DrawerOverlay, FormControl, FormLabel, NumberDecrementStepper, NumberIncrementStepper, NumberInput, NumberInputField, NumberInputStepper, Select, Stack, useDisclosure } from '@chakra-ui/react';
import { useRef, useState } from 'react';

export type DrawerStarshipFilters = {
    hyperdriveRating: string, 
    maxNumberOfPassengers: number
}

type Props = {
    onClose: () => void;
    onSubmit: ({ hyperdriveRating, maxNumberOfPassengers}: DrawerStarshipFilters) => void;
    isOpen: boolean;
}

export default function StarshipFilterDrawer({ isOpen, onClose, onSubmit }: Props) {
    const hyperdriveRatingOptions = ['0.5', '1.0', '2.0', '3.0', '4.0'];
    const [hyperdriveRating, setHyperdriveRating] = useState('0.5');
    const [maxNumberOfPassengers, setMaxNumberOfPassengers] = useState(0);


    return (
        <Drawer
            isOpen={isOpen}
            placement='right'
            onClose={onClose}
            size='md'
        >
            <DrawerOverlay />
            <DrawerContent>
                <DrawerCloseButton />
                <DrawerHeader borderBottomWidth='1px'>
                    Filter starships
                </DrawerHeader>

                <DrawerBody>
                    <Stack spacing='24px'>
                        <Box>
                            <FormControl>
                                <FormLabel>Max number of passengers</FormLabel>
                                <NumberInput max={500000} min={10} onChange={value => setMaxNumberOfPassengers(Number(value))}>
                                    <NumberInputField />
                                    <NumberInputStepper>
                                        <NumberIncrementStepper />
                                        <NumberDecrementStepper />
                                    </NumberInputStepper>
                                </NumberInput>
                            </FormControl>
                        </Box>

                        <Box>
                            <FormControl>
                                <FormLabel>Hyperdrive Rating</FormLabel>
                                <Select placeholder='Select hyperdrive rating' onChange={e => setHyperdriveRating(e.target.value)}>
                                    {hyperdriveRatingOptions.map((option, index) => {
                                        return <option key={index} selected={index === 0}>
                                            {option}
                                        </option>;
                                    })}
                                </Select>
                            </FormControl>
                        </Box>
                    </Stack>
                </DrawerBody>

                <DrawerFooter borderTopWidth='1px'>
                    <Button variant='outline' mr={3} onClick={onClose}>
                        Cancel
                    </Button>
                    <Button colorScheme='blue' isDisabled={!maxNumberOfPassengers} onClick={() => onSubmit({ hyperdriveRating, maxNumberOfPassengers })}>Submit</Button>
                </DrawerFooter>
            </DrawerContent>
        </Drawer>
    );
}
