'use client';
import { StarshipDetail } from '@/api/starships/schema/starshipSchema';
import { Box, Card, CardBody, CardHeader, Heading, Stack, StackDivider, Text } from '@chakra-ui/react';

type Props = {
  starshipDetail: StarshipDetail;
  isWithMoreDetail?: boolean;
}

export default function StarshipDetailCard({ starshipDetail, isWithMoreDetail = false }: Props) {
  return (
    <Card>
      <CardHeader>
        <Heading size='md'>{starshipDetail.name}</Heading>
      </CardHeader>

      <CardBody>
        <Stack divider={<StackDivider />} spacing='4'>
          <Box>
            <Heading size='xs' textTransform='uppercase'>
              Name :
            </Heading>
            <Text pt='2' fontSize='sm'>
              {starshipDetail.name}
            </Text>
          </Box>
          <Box>
            <Heading size='xs' textTransform='uppercase'>
              Model :
            </Heading>
            <Text pt='2' fontSize='sm'>
              {starshipDetail.model}
            </Text>
          </Box>
          <Box>
            <Heading size='xs' textTransform='uppercase'>
              Nb of passengers :
            </Heading>
            <Text pt='2' fontSize='sm'>
              {starshipDetail.passengers}
            </Text>
          </Box>
          {isWithMoreDetail && (
            <Stack divider={<StackDivider />} spacing='4'>
              <Box>
                <Heading size='xs' textTransform='uppercase'>
                  Consumables :
                </Heading>
                <Text pt='2' fontSize='sm'>
                  {starshipDetail?.consumables}
                </Text>
              </Box>
              <Box>
                <Heading size='xs' textTransform='uppercase'>
                  Crew :
                </Heading>
                <Text pt='2' fontSize='sm'>
                  {starshipDetail?.crew}
                </Text>
              </Box>
              <Box>
                <Heading size='xs' textTransform='uppercase'>
                  Hyperdrive rating :
                </Heading>
                <Text pt='2' fontSize='sm'>
                  {starshipDetail?.hyperdrive_rating}
                </Text>
              </Box>
            </Stack>)}
        </Stack>
      </CardBody>
    </Card>
  );
}
