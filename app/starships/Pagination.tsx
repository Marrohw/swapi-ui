'use client';
import { Button, Flex } from '@chakra-ui/react';

type Props = {
  nextPage: string;
  previousPage: string;
  onNextPage: () => void;
  onPreviousPage: () => void;
  currentPage: number;
}

export default function Pagination({ nextPage, previousPage, onNextPage, onPreviousPage, currentPage }: Props) {
  return (
    <Flex gap='4' alignItems='center' marginLeft='auto'>
      <Button isDisabled={!previousPage} onClick={onPreviousPage}>Prev</Button>
      <p>{currentPage}</p>
      <Button isDisabled={!nextPage} onClick={onNextPage}>Next</Button>
    </Flex>
  );
}
