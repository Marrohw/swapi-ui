'use client'
import { Starship, StarshipDetail } from '@/api/starships/schema/starshipSchema';
import Pagination from './Pagination';
import { useCallback, useEffect, useState } from 'react';
import { Text, Flex, Grid, Divider, useDisclosure, Button } from '@chakra-ui/react';
import { starshipApi } from '@/api/starships/starshipApi';
import StarshipFilterDrawer, { DrawerStarshipFilters } from './StarshipFilterDrawer';
import StarshipItem from './StarshipItem';


export default function StarshipsPage() {
  const [currentPage, setCurrentPage] = useState(1);
  const [starship, setStarship] = useState<Starship | null>(null);
  const [starshipFilters, setStarshipFilters] = useState<DrawerStarshipFilters | null>(null);

  const { isOpen, onOpen, onClose } = useDisclosure();

  // TODO : Extract this in a useGetStarships hook
  const getStarships = useCallback(async (url?: string) => {
    const res = await fetch(url ? url : starshipApi);
    const data = await res.json();
    if (!starshipFilters) {
      setStarship(data as Starship);
    } else {
      // TODO : Extract this in a "filter" util or hook 
      const results = data?.results.filter((s: StarshipDetail) => s.passengers < starshipFilters.maxNumberOfPassengers && s.hyperdrive_rating === starshipFilters.hyperdriveRating);
      setStarship({ ...data, count: results.length, results: results } as Starship);
    }
  }, [starshipFilters])

  useEffect(() => {
    getStarships().catch(console.error);
  }, [getStarships])

  return starship ? (
    <Flex direction='column' minW='740px'>
      <Flex alignItems='center' justifyContent='space-between'>
        <h1>Total number of starships : ({starship.count})</h1>
        <Button onClick={onOpen}>Add filter</Button>
      </Flex>
      <StarshipFilterDrawer onClose={onClose} isOpen={isOpen} onSubmit={(filters) => { onClose(); setStarshipFilters(filters) }} />
      <Divider my='4' />
      {starship.results.length > 0 ?
        <Grid templateColumns='repeat(5, 1fr)' gap={4}>
          {starship.results.map((result, index) => {
            return <StarshipItem index={index} starshipDetail={result} key={index} />;
          })}
        </Grid> : <Text>No starships found</Text>}
      <Pagination nextPage={starship.next ?? ''}
        previousPage={starship.previous ?? ''}
        currentPage={currentPage}
        onNextPage={() => { getStarships(starship.next ?? ''); setCurrentPage(currentPage + 1) }}
        onPreviousPage={() => { getStarships(starship.previous ?? ''); setCurrentPage(currentPage - 1) }} />
    </Flex>
  ) : null;
}
