'use client';
import { Box, Card, CardBody, Heading, Stack, StackDivider, Text } from '@chakra-ui/react';

type Props = {
    starshipFilms: string[];
}

export default function StarshipFilmCard({ starshipFilms }: Props) {
    return (
        <Card>
            <CardBody>
                <Stack divider={<StackDivider />} spacing='4'>
                    <Box>
                        <Heading size='xs' textTransform='uppercase'>
                            Featured in :
                        </Heading>
                        {starshipFilms.map((film, index) => {
                            return <Text pt='2' fontSize='sm' key={index}>
                                {film}
                            </Text>;
                        })}
                    </Box>
                </Stack>
            </CardBody>
        </Card>
    );
}
