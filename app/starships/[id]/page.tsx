'use client'
import { StarshipDetail } from '@/api/starships/schema/starshipSchema';
import { Flex } from '@chakra-ui/react';
import { useState, useCallback, useEffect } from 'react';
import Loading from './loading';
import { starshipApi } from '@/api/starships/starshipApi';
import StarshipDetailCard from '../StarshipDetailCard';
import StarshipFilmCard from '../StarshipFilmCard';

type Props = {
  params: { id: string }
}

export default function StarshipPage({ params }: Props) {
  const [starship, setStarship] = useState<StarshipDetail | null>(null);
  const [isLoading, setIsLoading] = useState(false);
  const [films, setFilms] = useState<string[]>([]);

  // TODO : Extract this in a useGetFilms hook
  const getFilms = useCallback(async (urls: string[]) => {
    const promises = urls.map(url => {
      return fetch(url)
        .then(response => {
          return response.json();
        });
    });

    Promise.all(promises).then(results => {
      const videos = results.map(result => result.title);
      setFilms(videos);
      setIsLoading(false);
    })
  }, [])

  // TODO : Extract this in a useGetStarshipById hook
  const getStarship = useCallback(async (id?: string) => {
    setIsLoading(true);
    const res = await fetch(
      `${starshipApi}${id}`,
      {
        next: { revalidate: 10 },
      }
    );
    const data = await res.json();
    const starshipDetail = data as StarshipDetail;
    setStarship(starshipDetail);
    getFilms(starshipDetail.films);
  }, [])

  useEffect(() => {
    getStarship(params.id).catch(console.error);
  }, [getStarship])

  return (isLoading ? <Loading /> : <Flex direction='column' gap='4'>
    <StarshipDetailCard starshipDetail={starship ?? {} as StarshipDetail} isWithMoreDetail />
    <StarshipFilmCard starshipFilms={films} />
  </Flex>
  );
}