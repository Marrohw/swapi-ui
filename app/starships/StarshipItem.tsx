import { StarshipDetail } from "@/api/starships/schema/starshipSchema";
import Link from "next/link";
import StarshipDetailCard from "./StarshipDetailCard";

type StarshipDetailWithIndex = {
    starshipDetail: StarshipDetail;
    index: number;
}

export default function Starship({ starshipDetail, index }: StarshipDetailWithIndex) {
    return (
        <Link href={`/starships/${starshipDetail.url.substring(starshipDetail.url.lastIndexOf('/') - 2).replace('/', '')}`} key={index}>
            <StarshipDetailCard starshipDetail={starshipDetail} />
        </Link>
    );
}