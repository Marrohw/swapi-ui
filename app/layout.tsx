'use client';
import Link from 'next/link';
import './globals.css';
import { ChakraProvider, Flex } from '@chakra-ui/react';

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html>
      <body>
      <ChakraProvider>
        <main>
        <Flex direction='column' alignItems='center' margin='auto' gap='2'>
          <nav>
            <Link href="/">
              Home
            </Link>
            <Link href="/starships">
              Starships
            </Link>
          </nav>
          {children}        
          </Flex>
        </main>
        </ChakraProvider>
      </body>
    </html>
    
  );
}